/*
 * CalcSheet.java
 * 
 * Version 2.0
 * 
 * Date 2008-03-12
 * Updated 2008-05-13
 * 
 * Copyright 2008 
 */

package pl.poznan.put.spio;

import java.io.IOException;
import java.util.List;

/**
 * The <code>CalcSheet</code> class is the entry point for the CalcSheet
 * program and is responsible for running the entire algorithm by creating
 * objects and using their services.
 * 
 * @author 
 * @author 
 * @version 2.0
 */
public class CalcSheet {

	/**
	 * Entry point for the CalcSheet program - it is responsible for
	 * initializing the objects necessary to conduct various operations in the
	 * course of processing the files.
	 * 
	 * <p>
	 * The program algorithm is run in three steps,
	 * <ul>
	 * <li>First the input data is read into the program using the
	 * <code>TupleReader</code>;</li>
	 * <li>Secondly the data is processed by the instance of the
	 * <code>Processor</code> class;</li>
	 * <li>Finally the results (table for variables and the strings
	 * representing operations) are collected and sent to the
	 * <code>TupleWriter</code> and output to file.</li>
	 * </ul>
	 * 
	 * @param args
	 *            array of parameters provided through command line
	 * @throws IOException
	 *             If an I/O error occurs during file access or output
	 */
	public static void main(String[] args) throws IOException {
		String inputFile = args[0];
		String outputFile = args[1];

		Processor processor = new Processor();
		TupleReader tupleReader = new TupleReader();
		TupleWriter tupleWriter = new TupleWriter();

		tupleReader.read(inputFile);
		processor
				.process(tupleReader.getTuples(), tupleReader.getExpressions());

		List<Long> results = processor.getResults();

		tupleWriter.appendTuple(results);
		tupleWriter.write(outputFile);
	}
}