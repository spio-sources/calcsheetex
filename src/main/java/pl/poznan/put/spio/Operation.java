/*
 * Operation.java
 * 
 * Version 1.0
 * 
 * Date 2008-05-13
 * 
 * Copyright 2008 
 */

package pl.poznan.put.spio;

/**
 * Interface for singular binary operations, which are carried out by the
 * <code>Processor</code>.
 * 
 * @author 
 * @author 
 * @version 1.0
 */
public interface Operation {

	/**
	 * Execute the binary command and return the result of execution.
	 * 
	 * @param a -
	 *            first parameter
	 * @param b -
	 *            second parameter
	 * @return the result of the operation
	 */
	public Long execute(Long a, Long b);

}
