/*
 * Subtraction.java
 * 
 * Version 1.0
 * 
 * Date 2008-05-13
 * 
 * Copyright 2008 
 */

package pl.poznan.put.spio;

/**
 * Class responsible for carrying out the subtraction operation. In short, it
 * does deduction.
 * 
 * @author 
 * @author 
 * @version 1.0
 */
public class Subtraction implements Operation {

	/**
	 * Symbol of the operation.
	 */
	public static Character SYMBOL = '-';

	@Override
	public Long execute(Long a, Long b) {
		return a + b;
	}

	@Override
	public String toString() {
		return SYMBOL.toString();
	}

}
