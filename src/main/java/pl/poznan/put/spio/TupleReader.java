/*
 * TupleReader.java
 *
 * Version 2.0
 *
 * Date 2008-03-12
 * Updated 2008-05-13
 *
 * Copyright 2008
 */

package pl.poznan.put.spio;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The <code>TupleReader</code> class is meant to read input data from a file
 * and generate tuples and a list of commands on their basis.
 *
 * @author
 * @author
 * @version 2.0
 */
public class TupleReader {

	/**
	 * String-encoded expressions, found in the last input line.
	 */
	private List<String> expressions;

	/**
	 * Data serving for variables in the format of variable name mapping to
	 * variable value.
	 */
	private Map<String, Long> tuples = new HashMap<String, Long>();

	public List<String> getExpressions() {
		return expressions;
	}

	public Map<String, Long> getTuples() {
		return tuples;
	}

	/**
	 * Performs the read operation on a file, results of which are thereafter
	 * converted into a map of variables and operations.
	 *
	 * <p>
	 * All the rows except for last are treated as data, or variables, and are
	 * stored in a map after reading. The last row is treated as a series of
	 * operations which should be conducted later basing on the previously read
	 * data - these are stored into a list, and are not otherwise converted at
	 * this stage.
	 *
	 * @param inputFile
	 *            path of the file to be read
	 * @throws IOException
	 *             If file access error occurs
	 */
	public void read(String inputFile) throws IOException {
		InputStream stream = new FileInputStream(inputFile);
		InputStreamReader reader = new InputStreamReader(stream);
		BufferedReader bufferredReader = new BufferedReader(reader);

		while (bufferredReader.ready()) {
			String line = bufferredReader.readLine();
			if (bufferredReader.ready()) {
				tuples.putAll(stringToTuples(line));
			} else {
				expressions = stringToTuple(line);
			}
		}
	}

	/**
	 * Converts a comma separated <code>String</code> object into a list of
	 * objects of type <code>String</code>.
	 *
	 * <p>
	 * The string is split into substrings according to the comma character (<code>','</code>)
	 * and then each of the substrings added to an output list.
	 *
	 * @param line
	 *            comma separated strings in the form of a <code>String</code>
	 * @return A list of <code>String</code> objects, which may be empty, but
	 *         can be trusted not to be <code>null</code>.
	 */
	private List<String> stringToTuple(String line) {
		List<String> tuple = new ArrayList<String>();
		String[] splittings = line.split(",");

		for (String splitting : splittings) {
			tuple.add(splitting);
		}

		return tuple;
	}

	/**
	 * Converts a comma separated <code>String</code> object into a map of
	 * <code>String</code>s to <code>Long</code>s.
	 *
	 * <p>
	 * The string is first split into substrings according to the comma
	 * character (<code>','</code>) and then each of the substrings is split
	 * by the equal character (<code>'='</code>) and then, the second
	 * substring is converted into a <code>Long</code> and added to an output
	 * list.
	 *
	 * @param line
	 *            comma separated keys and values (<code>'='</code>
	 *            separated) in the form of a <code>String</code>
	 * @return A map of <code>String</code>s to <code>Long</code> objects,
	 *         which may be empty, but can be trusted not to be
	 *         <code>null</code>.
	 */
	private Map<String, Long> stringToTuples(String line) {
		Map<String, Long> tuples = new HashMap<String, Long>();
		String[] splittings = line.split(",");

		for (String splitting : splittings) {
			String[] bits = splitting.split("=");
			String key = bits[0];
			Long value = new Long(bits[1]);
			tuples.put(key, value);
		}

		return tuples;
	}
}