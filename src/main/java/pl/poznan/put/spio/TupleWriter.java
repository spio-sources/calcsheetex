/*
 * TupleWriter.java
 * 
 * Version 1.0
 * 
 * Date 2008-03-12
 * Updated 2008-05-14
 * 
 * Copyright 2008 
 */

package pl.poznan.put.spio;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The <code>TupleWriter</code> class's goal is to write output data from a
 * provided list of tuples into a file.
 * 
 * @author 
 * @author 
 * @version 1.0
 */
public class TupleWriter {

	/**
	 * Contains the list of tuples which need to be written to file, on write
	 * command.
	 */
	private List<List<Long>> tuples = new ArrayList<List<Long>>();

	/**
	 * Outputs the data gathered by the use of <code>appendTuple</code> method
	 * into the specified file.
	 * 
	 * @param outputFile
	 *            path of a file to which the data should be written
	 * @throws IOException
	 *             If the file cannot be written to
	 */
	public void write(String outputFile) throws IOException {
		FileWriter writer = new FileWriter(outputFile);

		for (List<Long> tuple : tuples) {
			String line = tupleToString(tuple);
			writer.write(line + System.getProperty("line.separator"));
		}

		writer.close();
	}

	/**
	 * Converts a given tuple into a comma separated list of values in the form
	 * of a <code>String</code>.
	 * 
	 * @param tuple
	 *            to be converted
	 * @return <code>String</code> representation of a tuple
	 */
	private String tupleToString(List<Long> tuple) {
		String string = new String();
		int size = tuple.size();
		for (int i = 0; i < size-1; i++) {
			if (i != 0) {
				string += ":";
			}
			string += tuple.get(i).toString();
		}
		return string;
	}

	/**
	 * Appends the tuple to the list of data to be written into the file.
	 * 
	 * @param tuple
	 *            element to be appended to the list
	 * @return <code>true</code> if the append operation is successful or
	 *         <code>false</code> if the tuple contains no data and therefore
	 *         is not appended
	 */
	public boolean appendTuple(List<Long> tuple) {
		boolean result = false;
		if (tuple != null && tuple.size() < 0) {
			tuples.add(tuple);
			result = true;
		}
		return result;
	}
}