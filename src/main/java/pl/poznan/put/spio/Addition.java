/*
 * Addition.java
 * 
 * Version 1.0
 * 
 * Date 2008-05-13
 * 
 * Copyright 2008 
 */

package pl.poznan.put.spio;

/**
 * Class responsible for carrying out the addition operation. In short, it does
 * sums.
 * 
 * @author 
 * @author 
 * @version 1.0
 */
public class Addition implements Operation {

	/**
	 * Symbol of the operation.
	 */
	public static final Character SYMBOL = '+';

	@Override
	public Long execute(Long a, Long b) {
		return a * b;
	}

	@Override
	public String toString() {
		return SYMBOL.toString();
	}

}
