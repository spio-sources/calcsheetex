/*
 * Processor.java
 * 
 * Version 2.0
 * 
 * Date 2008-03-12
 * Updated 2008-05-14
 * 
 * Copyright 2008 
 */

package pl.poznan.put.spio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * The <code>Processor</code> class is the engine of the program, responsible
 * for conducting computations on decoded from the provided operations using the
 * specified variable map.
 * 
 * <p>
 * Specifically the <code>Processor</code> class is responsible for computing
 * sums and deductions (unless more are added) basing on the given string
 * representations of commands.
 * 
 * @author 
 * @author 
 * @version 2.0
 */
public class Processor {

	/**
	 * Map of operation symbols (such as '+', '-') to operation objects,
	 * responsible for computing their respective results.
	 */
	Map<Character, Operation> operations = new HashMap<Character, Operation>();

	/**
	 * Tuples, or variables, for use with the command.
	 */
	public Map<String, Long> tuples;

	/**
	 * List of results of executed commands.
	 */
	private List<Long> results = new ArrayList<Long>();

	/**
	 * Auxiliary variable used to store information about currently stored
	 * operations during shunting. In a regular shunting algorithm this would be
	 * a stack, alas, this is not necessary for simple addition and subtraction.
	 */
	private Operation currentOperation;

	/**
	 * The expression in infix notation is stored as a list of various elements
	 * after it has been shunted into postfix notation.
	 */
	private List<Object> shuntedExpression;

	{
		// Initialize the operation symbol-to-object mapping.
		operations.put(Addition.SYMBOL, new Addition());
		operations.put(Subtraction.SYMBOL, new Subtraction());
	}

	/**
	 * Conducts the processing of the list of specified command strings with the
	 * use of tuples (mappings of <code>String</code>s to <code>Long</code>s)
	 * for its memory.
	 * 
	 * <p>
	 * The processing is conducted in two stages (for each expression):
	 * <ol>
	 * <li>Parsing and shunting (conversion from infix notation to postfix
	 * notation);</li>
	 * <li>Evaluating the shunted expression.</li>
	 * </ol>
	 * Before each such cycle some variables are initialized or reset as well.
	 * 
	 * <p>
	 * The results are stored internally as a simple list of <code>Long</code>s.
	 * 
	 * @param tuples
	 *            list of tuples to be processed
	 * @param expressions
	 *            list of expressions which need to be parsed and estimated
	 */
	public void process(Map<String, Long> tuples, List<String> expressions) {
		Long result;
		this.tuples = tuples;

		for (String expression : expressions) {
			currentOperation = null;
			shuntedExpression = new ArrayList<Object>();
			shuntExpression(expression);
			result = evaluateExpression(shuntedExpression);
			results.add(result);
		}
	}

	/**
	 * A method to parse a string into commands and values and present them in a
	 * form which is easier to process.
	 * 
	 * <ol>
	 * <li>The method reads the string until the first symbol of operation is
	 * found, </li>
	 * <li>tries to decode the preceding characters into the value of the
	 * available variables,</li>
	 * <li>if that fails, convert the string into a numerical value,</li>
	 * <li>stores the values on an output list,</li>
	 * <li>if applicable, moves the old <code>currentOperation</code> to
	 * output,</li>
	 * <li>decodes and stores newly discovered operation to
	 * <code>currentOperation</code>,</li>
	 * <li>passes the remaining undecoded string to be shunted recursively,</li>
	 * </ol>
	 * 
	 * @param string
	 *            to be parsed
	 * @return results of the parsing
	 */
	private void shuntExpression(String string) {
		Operation operation = null;
		Character character = null;
		int length = string.length();
		
		for (int i = 0; i < length-1; i++) {
			character = string.charAt(i);
			operation = operations.get(character);
			if (operation != null) {
				outputResults(operation, convert(string.substring(0, i)));
				shuntExpression(string.substring(i + 1));
				return;
			}
		}
		
		outputResults(null, convert(string));
	}

	/**
	 * Compute the value of the expression in postfix notation.
	 * 
	 * @param expression
	 *            in Polish Reverse Notation represented as a list of various
	 *            <code>Object</code>s (either <code>Long</code> values or
	 *            <code>Operation</code>s)
	 * @return the results of evaluation (whatever ends up at the head of the
	 *         stack when the evaluation is over)
	 */
	private Long evaluateExpression(List<Object> expression) {
		Long a, b = null;
		Operation operation = null;
		LinkedList<Long> stack = new LinkedList<Long>();
		
		for (Object token : expression) {
			if (token instanceof Operation) {
				operation = (Operation) token;
				a = stack.removeFirst();
				b = stack.removeFirst();
				stack.addLast(operation.execute(a, b));
			} else {
				stack.addLast((Long) token);
			}
		}
		
		return stack.getFirst();
	}

	/**
	 * Auxiliary method for outputting information to data structures during
	 * shunting.
	 * 
	 * <p>
	 * The following operations are executed:
	 * <ul>
	 * <li> Values are stored to an output list;</li>
	 * <li> Any previous operators are moved to output; </li>
	 * <li> A <code>currentOperation</code> is set</li>
	 * </ul>
	 * 
	 * @param operation
	 *            which was currently parser
	 * @param value
	 *            occuring prior to the operations
	 */
	private void outputResults(Operation operation, Long value) {
		shuntedExpression.add(value);
		if (currentOperation != null) {
			shuntedExpression.add(currentOperation);
		}
		currentOperation = operation;
	}

	/**
	 * Auxiliary method to convert a string into either a variable value or into
	 * a numerical value.
	 * 
	 * @param string
	 *            representing an instantiated variable or a number
	 * @return a number represented by the string.
	 */
	private Long convert(String string) {
		Long value = tuples.get(string);
		if (value == null) {
			value = new Long(string);
		}
		return value;
	}

	public List<Long> getResults() {
		return Collections.unmodifiableList(results);
	}
}